const std = @import("std");
pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    const alloc = gpa.allocator();

    const stdin = std.io.getStdIn();

    std.debug.print("Enter name: ", .{});
    var name = try stdin.reader().readUntilDelimiterAlloc(alloc, '\n', 1024);
    std.debug.print("Enter age: ", .{});
    const age = try stdin.reader().readUntilDelimiterAlloc(alloc, '\n', 1024);
    defer alloc.free(name);
    defer alloc.free(age);

    const age1 = std.fmt.parseInt(i32, age, 10);

    std.debug.print("name: {s}\n", .{name});
    std.debug.print("age: {any}\n", .{age1});
}
