const std = @import("std");
const stdout = std.io.getStdOut().writer();

fn swap_temp(a: i8, b: i8) !void {
    var c: i8 = a;
    var d: i8 = b;

    c = c + d;
    d = c - d;
    c = c - d;

    try stdout.print("The swapped value of A: {d} and B: {d}\n", .{ c, d });
}

fn xor_swap(a: i8, b: i8) !void {
    var x: i8 = a;
    var y: i8 = b;

    x = x ^ y;
    y = x ^ y;
    x = x ^ y;

    try stdout.print("The values swapped using xor operator: A = {d} and B = {d}\n", .{ x, y });
}

pub fn main() !void {
    const a = @as(i8, 10);
    const b = @as(i8, 20);

    try stdout.print("The values before swapped A: {d} and B: {d}\n", .{ a, b });

    //temp var fn call
    try swap_temp(a, b);

    //bitwise xor fn call
    try xor_swap(a, b);
}
