const std = @import("std");
const out = std.io.getStdOut().writer();

pub fn main() !void {
    const a = 20;
    const b = 30;

    try out.print("before swap of a = {d} and b = {d}\n", .{ a, b });

    swap(a, b) catch unreachable;
}

fn swap(a: i8, b: i8) !void {
    var x: i8 = a;
    var y: i8 = b;
    var z: i8 = undefined;

    z = x;
    x = y;
    y = z;

    try out.print("after swap of a = {d} and b = {d}\n", .{ x, y });
}
